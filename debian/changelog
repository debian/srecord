srecord (1.64-4) unstable; urgency=medium

  * QA upload.
  * debian/control: 'debhelper-compat' in Build-Depends is bumped to level 13.
  * debian/control: Bumped Standards-Version to 4.6.2.
  * debian/rules: Added compilation option to enable "bindnow" in linker.
  * debian/patches: Created 01-fix-typos-in-manpages.patch
    to fix typos in the manpages.

 -- Aquila Macedo Costa <aquilamacedo@riseup.net>  Wed, 21 Jun 2023 18:48:23 -0300

srecord (1.64-3) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + libsrecord-dev: Drop versioned constraint on srecord in Replaces.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 22 Aug 2021 03:10:52 +0100

srecord (1.64-2) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Update standards version to 4.2.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 13 Feb 2021 00:59:18 +0000

srecord (1.64-1) unstable; urgency=medium

  * QA upload.
  * New upstream release (Closes: #859701).
  * Set Maintainer to Debian QA Group (see #879437).
  * Update watch file to version 4 format.
  * Upgrade to debhelper compat level 11.
  * Remove debug package from debian/control.
  * Upgrade to Standards-Version 4.2.0.
  * Set Priority to optional (extra is deprecated).
  * Update debian/copyright to the machine-readable format.
  * Depend on libgcrypt20-dev instead of libgcrypt11-dev
    (Closes: #864136).
  * Delete trailing whitespace from debian/changelog.
  * Adjust debian/*.install files.
  * Add patch 00-fix-whatis.patch to fix the whatis entry for
    srec_input.
  * Update Vcs-Git and Vcs-Browser fields to new Salsa URL.

 -- Fabian Wolff <fabi.wolff@arcor.de>  Fri, 10 Aug 2018 18:39:02 +0200

srecord (1.58-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Build-depend on libtool-bin. Closes: #761787.

 -- Matthias Klose <doko@debian.org>  Thu, 09 Oct 2014 10:08:46 +0000

srecord (1.58-1) unstable; urgency=low

  * New upstream release.

 -- Fathi Boudra <fabo@debian.org>  Sat, 31 Dec 2011 12:54:00 +0200

srecord (1.56-1) unstable; urgency=low

  * New upstream release.

 -- Fathi Boudra <fabo@debian.org>  Wed, 06 Oct 2010 15:54:20 +0300

srecord (1.55-1) unstable; urgency=low

  * New upstream release.
  * Remove 01_ftbfs_create_libdir_directory.diff - fixed upstream.

 -- Fathi Boudra <fabo@debian.org>  Wed, 10 Mar 2010 15:32:30 +0100

srecord (1.54-1) unstable; urgency=low

  * New upstream release.
  * Switch to dpkg-source 3.0 (quilt) format.
  * Remove 05_remove_license_manpage.diff - skipped in install file.
  * Add 01_ftbfs_create_libdir_directory.diff:
    fix ftbfs due to libdir directory not created.
  * Improve debian/copyright:
    - add missing copyrights.
    - update copyright years.
    - add srecord library license (LGPL-3).
  * Update debian/control:
    - add libtool build dependency.
    - remove quilt build dependency.
    - add srecord library packages
      (libsrecord-dev, libsrecord0 and libsrecord0-dbg).
    - add Replaces against previous srecord versions as srecord.3 manpage is
      moved to libsrecord-dev package.
  * Update debian/rules:
    - enable debug package (pass --dbg-package option to dh).
    - remove quilt option passed to dh.

 -- Fathi Boudra <fabo@debian.org>  Sat, 30 Jan 2010 09:30:05 +0100

srecord (1.52-1) unstable; urgency=low

  * New upstream release.
  * Remove 01_gcc_4.4_support.diff - merged upstream.
  * Update debian/control:
    - bump Standards-Version from 3.8.2 to 3.8.3 (no changes needed).

 -- Fathi Boudra <fabo@debian.org>  Sun, 04 Oct 2009 11:53:54 +0200

srecord (1.50-1) unstable; urgency=low

  * New upstream release.
  * Add patch to fix ftbfs with gcc-4.4. (Closes: #504851)
  * Remove patches:
    - 01_gcc_4.3_support.diff - merged upstream.
    - 02_remove_useless_cstdio.diff - merged upstream.
    - 03_groff_fix.diff - merged upstream.
    - 04_section_mismatch.diff - merged upstream.
  * Bump debian/compat from 6 to 7.
  * Update debian/control:
    - remove cdbs build dependency.
    - add libboost-dev and libgcrypt11-dev build dependencies.
    - bump debhelper version to 7.3.
    - bump Standards-Version from 3.7.3 to 3.8.2 (no changes needed).
  * Convert debian/rules to dh usage.
  * Fix debian/watch file.

 -- Fathi Boudra <fabo@debian.org>  Mon, 10 Aug 2009 14:14:39 +0200

srecord (1.39-1) unstable; urgency=low

  * New maintainer. (Closes: #465950)
  * New upstream release. (Closes: #440381)
  * Add patch to fix ftbfs with gcc-4.3. (Closes: #417703)

 -- Fathi Boudra <fabo@debian.org>  Fri, 15 Feb 2008 18:17:34 +0100

srecord (1.24-1) unstable; urgency=low

  * New Upstream Version
  * Fixed manpage (lintian,man warnings)

 -- Stephen M Moraco <stephen@debian.org>  Sun, 26 Mar 2006 18:37:02 -0700

srecord (1.23-1) unstable; urgency=low

  * New Upstream
  * Added watch file
  * Update Policy Standards Version

 -- Stephen M Moraco <stephen@debian.org>  Wed, 28 Sep 2005 21:00:32 -0600

srecord (1.21-1) unstable; urgency=low

  * New Upstream Source
  * Fix new typecast syntax error (Closes: Bug#263822)
  * Fix lintian manpage errors

 -- Stephen M Moraco <stephen@debian.org>  Mon, 11 Apr 2005 17:40:57 -0600

srecord (1.20-2) unstable; urgency=low

  * Fix parm in header of aomf.h (Closes: Bug#232246)
    forwarded this fix to upstream

 -- Stephen M Moraco <stephen@debian.org>  Wed, 18 Feb 2004 08:29:29 -0700

srecord (1.20-1) unstable; urgency=low

  * New Upstream Version

 -- Stephen M Moraco <stephen@debian.org>  Mon, 16 Feb 2004 23:54:09 -0700

srecord (1.19-1) unstable; urgency=low

  * New Upstream Version

 -- Stephen M Moraco <stephen@debian.org>  Sat, 24 Jan 2004 23:11:59 -0700

srecord (1.16-1) unstable; urgency=low

  * New Upstream Version
  * Update to latest policy version

 -- Stephen M Moraco <stephen@debian.org>  Sun,  7 Sep 2003 12:32:16 -0600

srecord (1.14-1) unstable; urgency=low

  * New Upstream Version
  * Update Policy Version
  * Update per debhelper best practice changes

 -- Stephen M Moraco <stephen@debian.org>  Wed, 12 Mar 2003 23:49:06 -0700

srecord (1.10-2) unstable; urgency=low

  * Update to latest policy version
  * Remove incorrect postinst/prerm scripts

 -- Stephen M Moraco <stephen@debian.org>  Tue,  1 Oct 2002 18:01:19 -0600

srecord (1.10-1) unstable; urgency=low

  * Now builds with g++ 3.x (Closes: Bug#148708)
  * See /usr/share/doc/README.gz for latest release notes
  * New Upstream Version

 -- Stephen M Moraco <stephen@debian.org>  Fri, 26 Jul 2002 21:21:44 -0600

srecord (1.9-5) unstable; urgency=low

  * New Maintainer (Closes: Bug#138940)
  * Close 2nd dupe? WNPP bug too (Closes: Bug#126005)

 -- Stephen M Moraco <stephen@debian.org>  Wed, 17 Apr 2002 00:47:38 -0600

srecord (1.9-4) unstable; urgency=low

  * Changing owner to packages@qa.debian.org and orphaning
  * I may (repeat may) reimplement srecord in perl.

 -- Vince Mulhollon <vlm@debian.org>  Fri, 29 Mar 2002 20:03:30 -0600

srecord (1.9-3) unstable; urgency=low

  * HPPA build still fails on new version of srecord.
    Problem was, string is now in std:: namespace because of GCC 3.0
    Thus, I spent one night changing a large number of namespaces.
  * Still taking errors on HPPA due to a problem I don't understand
    revolving around virtual functions.  I'll release this and then
    when I ask for help on HPPA people will have a common ground.

 -- Vince Mulhollon <vlm@debian.org>  Fri, 25 Jan 2002 22:09:29 -0600

srecord (1.9-2) unstable; urgency=low

  * Added build-depend on groff (closes: Bug#126710)

 -- Vince Mulhollon <vlm@debian.org>  Wed,  2 Jan 2002 18:29:57 -0600

srecord (1.9-1) unstable; urgency=low

  * New upstream release
  * Change priority from optional to extra
  * Changed architecture to any so as not to exclude HPPA
  * Modifed the control file decription to reflect the large number
    of new capabilities.
  * Updated copyright file.  The basic information contained does not
    change, it's still GPL, etc.  I cleaned up the file by using the
    authors exact words cut and paste, and updated the URLs.

 -- Vince Mulhollon <vlm@debian.org>  Mon, 24 Dec 2001 17:11:50 -0600

srecord (1.5-2) unstable; urgency=low

  * srecord is seriously out of date.  Upstream is at version 1.9
    My plan is to circle the wagons and patch up 1.5 as much as
    possible.  Then I'll focus on getting a new 1.9 out the door.
  * Regarding above, I think getting 1.5 up on HPPA is a lost
    cause.  If someone gives me a patch for 1.5, I'll apply it
    gladly with a "Thank You".  1.9 is enough of a rewrite,
    that it's practically a whole new program anyway.
    There is also the practical point that if no one has gotten
    srecord 1.5 working on HPPA in 160 days, I'm sure I won't either,
    and furthermore, I'd be better off getting 1.9 out the door
    for all architectures (including HPPA) rather than work on
    only HPPA (closes: Bug#105038)
  * New Maintainer (closes: Bug#123511)
  * IA64 patch from 106220 applied (closes: Bug#106220)
  * Compliant with policy version 3.5.6.0


 -- Vince Mulhollon <vlm@debian.org>  Thu, 20 Dec 2001 17:27:49 -0600

srecord (1.5-1.1) unstable; urgency=low

  * NMU
  * In lib/srec/record.cc include string.h to satisfy newer and
    stricter versions of gcc.  (closes: Bug#106220)

 -- Doug Porter <dsp@debian.org>  Thu, 25 Oct 2001 16:26:23 -0500

srecord (1.5-1) unstable; urgency=low

  * New upstream version.

 -- Raphael Bossek <bossekr@debian.org>  Sun, 12 Mar 2000 10:45:55 +0100

srecord (1.4-1) unstable; urgency=low

  * New upstream version.

 -- Raphael Bossek <bossekr@debian.org>  Sun, 16 Jan 2000 10:59:59 +0100
