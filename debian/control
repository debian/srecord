Source: srecord
Section: utils
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 13),
               groff,
               libboost-dev,
               libgcrypt20-dev,
               libtool-bin,
               ghostscript
Standards-Version: 4.6.2
Homepage: http://srecord.sourceforge.net
Vcs-Browser: https://salsa.debian.org/debian/srecord
Vcs-Git: https://salsa.debian.org/debian/srecord.git/

Package: srecord
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: collection of powerful tools for manipulating EPROM load files
 The srecord package is a collection of powerful tools for manipulating EPROM
 load files. It reads and writes numerous EPROM file formats, and can perform
 many different manipulations.
 .
 The tools include:
  * The srec_cat program may be used to catenate (join) EPROM load files, or
    portions of EPROM load files, together. Because it understands all of the
    input and output formats, it can also be used to convert files from one
    format to another. It also understands filters.
  * The srec_cmp program may be used to compare EPROM load files, or portions
    of EPROM load files, for equality.
  * The srec_info program may be used to print summary information about EPROM
    load files.

Package: libsrecord-dev
Section: libdevel
Multi-Arch: same
Architecture: any
Depends: libsrecord0 (= ${binary:Version}), ${misc:Depends}
Description: library for manipulating EPROM load files (development)
 The srecord library is a collection of powerful C++ classes for manipulating
 EPROM load files. It reads and writes numerous EPROM file formats, and can
 perform many different manipulations.
 .
 This package contains the static libraries and headers for developing
 applications that use the srecord library.

Package: libsrecord0
Section: libs
Multi-Arch: same
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: library for manipulating EPROM load files (runtime)
 The srecord library is a collection of powerful C++ classes for manipulating
 EPROM load files. It reads and writes numerous EPROM file formats, and can
 perform many different manipulations.
 .
 This package contains the files necessary for running applications that
 use the srecord library.
